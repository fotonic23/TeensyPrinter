TeensyPrinter
=============

**WORK IN PROGRESS**

This is a RAMPS-like 3D printer control board that uses a [Teensy 3.6](https://www.pjrc.com/store/teensy36.html)
as its controller, instead of the more usual Arduino Mega or other options. 
Right now, I've planned out the following capabilities:

* three motor controller sockets for X/Y/Z
* three motor conrtoller sockets for up to three extruders
* jumper-selectable SPI and soft-endstop wiring for TMC2130 drivers
* min/max endstop inputs on all three axes
* thermistor inputs for bed, up to 3 extruders, and enclosure
* heater outputs for bed and up to 3 extruders
* part-cooling fan output
* built-in connection for a graphical LCD with encoder knob and SD slot
* two servo outputs
* one uncommitted analog input
* three uncommitted digital I/O lines, two with PWM

This uses up all available I/O on the Teensy 3.6.  The uncommitted analog
input would be usable for a filament-width sensor, while the uncommitted
digital I/O could be used for power-supply control, a filament-runout
sensor, or solenoid control (with some external electronics).  If you only
need X/Y/Z-min endstop inputs, the X/Y/Z-max inputs could also be used for
additional digital I/O lines.

