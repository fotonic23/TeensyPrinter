EESchema Schematic File Version 4
LIBS:TeensyPrinter-cache
EELAYER 29 0
EELAYER END
$Descr USLetter 11000 8500
encoding utf-8
Sheet 5 10
Title "Teensy Pololu Shield"
Date "2019-03-28"
Rev "0.1"
Comp "Scott Alfter"
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L Connector_Generic:Conn_01x03 J20
U 1 1 5C9B10DF
P 4750 3350
F 0 "J20" H 4830 3346 50  0000 L CNN
F 1 "Conn_01x03" H 4830 3301 50  0001 L CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x03_P2.54mm_Vertical" H 4750 3350 50  0001 C CNN
F 3 "~" H 4750 3350 50  0001 C CNN
F 4 "METZ CONNECT USA Inc." H 0   0   50  0001 C CNN "DK_Mfr"
F 5 "PR20203VBNN" H 0   0   50  0001 C CNN "DK_Mfr_PN"
F 6 "1849-1003-ND" H 0   0   50  0001 C CNN "DK_PN"
F 7 "any" H 0   0   50  0001 C CNN "Src Any/Spec"
F 8 "1" H 0   0   50  0001 C CNN "Qty Per Unit"
	1    4750 3350
	1    0    0    -1  
$EndComp
$Comp
L Connector_Generic:Conn_01x03 J22
U 1 1 5C9B262A
P 4750 3800
F 0 "J22" H 4830 3796 50  0000 L CNN
F 1 "Conn_01x03" H 4830 3751 50  0001 L CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x03_P2.54mm_Vertical" H 4750 3800 50  0001 C CNN
F 3 "~" H 4750 3800 50  0001 C CNN
F 4 "METZ CONNECT USA Inc." H 0   0   50  0001 C CNN "DK_Mfr"
F 5 "PR20203VBNN" H 0   0   50  0001 C CNN "DK_Mfr_PN"
F 6 "1849-1003-ND" H 0   0   50  0001 C CNN "DK_PN"
F 7 "any" H 0   0   50  0001 C CNN "Src Any/Spec"
F 8 "1" H 0   0   50  0001 C CNN "Qty Per Unit"
	1    4750 3800
	1    0    0    -1  
$EndComp
$Comp
L Connector_Generic:Conn_01x03 J24
U 1 1 5C9B352C
P 4750 4250
F 0 "J24" H 4830 4246 50  0000 L CNN
F 1 "Conn_01x03" H 4830 4201 50  0001 L CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x03_P2.54mm_Vertical" H 4750 4250 50  0001 C CNN
F 3 "~" H 4750 4250 50  0001 C CNN
F 4 "METZ CONNECT USA Inc." H 0   0   50  0001 C CNN "DK_Mfr"
F 5 "PR20203VBNN" H 0   0   50  0001 C CNN "DK_Mfr_PN"
F 6 "1849-1003-ND" H 0   0   50  0001 C CNN "DK_PN"
F 7 "any" H 0   0   50  0001 C CNN "Src Any/Spec"
F 8 "1" H 0   0   50  0001 C CNN "Qty Per Unit"
	1    4750 4250
	1    0    0    -1  
$EndComp
$Comp
L Connector_Generic:Conn_01x03 J21
U 1 1 5C9B3CB9
P 5750 3350
F 0 "J21" H 5850 3350 50  0000 C CNN
F 1 "Conn_01x03" H 5668 3576 50  0001 C CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x03_P2.54mm_Vertical" H 5750 3350 50  0001 C CNN
F 3 "~" H 5750 3350 50  0001 C CNN
F 4 "METZ CONNECT USA Inc." H 0   0   50  0001 C CNN "DK_Mfr"
F 5 "PR20203VBNN" H 0   0   50  0001 C CNN "DK_Mfr_PN"
F 6 "1849-1003-ND" H 0   0   50  0001 C CNN "DK_PN"
F 7 "any" H 0   0   50  0001 C CNN "Src Any/Spec"
F 8 "1" H 0   0   50  0001 C CNN "Qty Per Unit"
	1    5750 3350
	-1   0    0    -1  
$EndComp
$Comp
L Connector_Generic:Conn_01x03 J23
U 1 1 5C9B4C6A
P 5750 3800
F 0 "J23" H 5850 3800 50  0000 C CNN
F 1 "Conn_01x03" H 5668 4026 50  0001 C CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x03_P2.54mm_Vertical" H 5750 3800 50  0001 C CNN
F 3 "~" H 5750 3800 50  0001 C CNN
F 4 "METZ CONNECT USA Inc." H 0   0   50  0001 C CNN "DK_Mfr"
F 5 "PR20203VBNN" H 0   0   50  0001 C CNN "DK_Mfr_PN"
F 6 "1849-1003-ND" H 0   0   50  0001 C CNN "DK_PN"
F 7 "any" H 0   0   50  0001 C CNN "Src Any/Spec"
F 8 "1" H 0   0   50  0001 C CNN "Qty Per Unit"
	1    5750 3800
	-1   0    0    -1  
$EndComp
$Comp
L Connector_Generic:Conn_01x03 J25
U 1 1 5C9B51F4
P 5750 4250
F 0 "J25" H 5850 4250 50  0000 C CNN
F 1 "Conn_01x03" H 5668 4476 50  0001 C CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x03_P2.54mm_Vertical" H 5750 4250 50  0001 C CNN
F 3 "~" H 5750 4250 50  0001 C CNN
F 4 "METZ CONNECT USA Inc." H 0   0   50  0001 C CNN "DK_Mfr"
F 5 "PR20203VBNN" H 0   0   50  0001 C CNN "DK_Mfr_PN"
F 6 "1849-1003-ND" H 0   0   50  0001 C CNN "DK_PN"
F 7 "any" H 0   0   50  0001 C CNN "Src Any/Spec"
F 8 "1" H 0   0   50  0001 C CNN "Qty Per Unit"
	1    5750 4250
	-1   0    0    -1  
$EndComp
Text GLabel 4400 3450 0    50   Output ~ 0
X_MIN
Wire Wire Line
	4550 3350 4500 3350
Wire Wire Line
	4500 3350 4500 3800
Wire Wire Line
	4500 3800 4550 3800
Wire Wire Line
	4500 3800 4500 4250
Wire Wire Line
	4500 4250 4550 4250
Connection ~ 4500 3800
Wire Wire Line
	4500 4250 4500 4550
Connection ~ 4500 4250
Wire Wire Line
	4550 4150 4450 4150
Wire Wire Line
	4450 4150 4450 3700
Wire Wire Line
	4450 3700 4550 3700
Wire Wire Line
	4450 3700 4450 3250
Wire Wire Line
	4450 3250 4550 3250
Connection ~ 4450 3700
Wire Wire Line
	4450 3250 4450 3100
Connection ~ 4450 3250
Wire Wire Line
	4400 3450 4550 3450
Text GLabel 4400 3900 0    50   Output ~ 0
Y_MIN
Text GLabel 4400 4350 0    50   Output ~ 0
Z_MIN
Wire Wire Line
	4400 4350 4550 4350
Wire Wire Line
	4400 3900 4550 3900
$Comp
L power:+3.3V #PWR053
U 1 1 5C9B6A3F
P 4450 3100
F 0 "#PWR053" H 4450 2950 50  0001 C CNN
F 1 "+3.3V" H 4465 3273 50  0000 C CNN
F 2 "" H 4450 3100 50  0001 C CNN
F 3 "" H 4450 3100 50  0001 C CNN
	1    4450 3100
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR055
U 1 1 5C9B6BC6
P 4500 4550
F 0 "#PWR055" H 4500 4300 50  0001 C CNN
F 1 "GND" H 4505 4377 50  0000 C CNN
F 2 "" H 4500 4550 50  0001 C CNN
F 3 "" H 4500 4550 50  0001 C CNN
	1    4500 4550
	1    0    0    -1  
$EndComp
Wire Wire Line
	5950 3350 6000 3350
Wire Wire Line
	6000 3350 6000 3800
Wire Wire Line
	6000 3800 5950 3800
Wire Wire Line
	6000 3800 6000 4250
Wire Wire Line
	6000 4250 5950 4250
Connection ~ 6000 3800
$Comp
L power:GND #PWR056
U 1 1 5C9B778B
P 6000 4550
F 0 "#PWR056" H 6000 4300 50  0001 C CNN
F 1 "GND" H 6005 4377 50  0000 C CNN
F 2 "" H 6000 4550 50  0001 C CNN
F 3 "" H 6000 4550 50  0001 C CNN
	1    6000 4550
	1    0    0    -1  
$EndComp
Wire Wire Line
	6000 4550 6000 4250
Connection ~ 6000 4250
$Comp
L power:+3.3V #PWR054
U 1 1 5C9B8605
P 6050 3100
F 0 "#PWR054" H 6050 2950 50  0001 C CNN
F 1 "+3.3V" H 6065 3273 50  0000 C CNN
F 2 "" H 6050 3100 50  0001 C CNN
F 3 "" H 6050 3100 50  0001 C CNN
	1    6050 3100
	1    0    0    -1  
$EndComp
Wire Wire Line
	6050 3100 6050 3250
Wire Wire Line
	6050 3250 5950 3250
Wire Wire Line
	6050 3250 6050 3700
Wire Wire Line
	6050 3700 5950 3700
Connection ~ 6050 3250
Wire Wire Line
	6050 3700 6050 4150
Wire Wire Line
	6050 4150 5950 4150
Connection ~ 6050 3700
Text GLabel 6100 3450 2    50   Output ~ 0
X_MAX
Wire Wire Line
	5950 3450 6100 3450
Text GLabel 6100 3900 2    50   Output ~ 0
Y_MAX
Text GLabel 6100 4350 2    50   Output ~ 0
Z_MAX
Wire Wire Line
	5950 3900 6100 3900
Wire Wire Line
	5950 4350 6100 4350
$EndSCHEMATC
