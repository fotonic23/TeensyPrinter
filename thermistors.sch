EESchema Schematic File Version 4
LIBS:TeensyPrinter-cache
EELAYER 29 0
EELAYER END
$Descr USLetter 11000 8500
encoding utf-8
Sheet 6 10
Title "Teensy Pololu Shield"
Date "2019-03-28"
Rev "0.1"
Comp "Scott Alfter"
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L Device:R_US R20
U 1 1 5CA6E900
P 4850 3700
F 0 "R20" H 4918 3746 50  0000 L CNN
F 1 "4.7k" H 4918 3655 50  0000 L CNN
F 2 "Resistor_SMD:R_0805_2012Metric" V 4890 3690 50  0001 C CNN
F 3 "~" H 4850 3700 50  0001 C CNN
F 4 "Stackpole " H 0   0   50  0001 C CNN "DK_Mfr"
F 5 "RMCF0805FT4K70" H 0   0   50  0001 C CNN "DK_Mfr_PN"
F 6 "RMCF0805FT4K70CT-ND" H 0   0   50  0001 C CNN "DK_PN"
F 7 "0805" H 0   0   50  0001 C CNN "Package"
F 8 "any" H 0   0   50  0001 C CNN "Src Any/Spec"
F 9 "1" H 0   0   50  0001 C CNN "Qty Per Unit"
	1    4850 3700
	1    0    0    -1  
$EndComp
$Comp
L Device:CP1 C9
U 1 1 5CA6EE4C
P 4850 4100
F 0 "C9" H 4965 4146 50  0000 L CNN
F 1 "10u" H 4965 4055 50  0000 L CNN
F 2 "Capacitor_SMD:CP_Elec_4x5.4" H 4850 4100 50  0001 C CNN
F 3 "~" H 4850 4100 50  0001 C CNN
F 4 "Würth" H 0   0   50  0001 C CNN "DK_Mfr"
F 5 "865090140001" H 0   0   50  0001 C CNN "DK_Mfr_PN"
F 6 "732-8385-1-ND" H 0   0   50  0001 C CNN "DK_PN"
F 7 "4.0x5.4" H 0   0   50  0001 C CNN "Package"
F 8 "any" H 0   0   50  0001 C CNN "Src Any/Spec"
F 9 "1" H 0   0   50  0001 C CNN "Qty Per Unit"
	1    4850 4100
	1    0    0    -1  
$EndComp
Wire Wire Line
	4850 3850 4850 3900
$Comp
L power:+3.3V #PWR058
U 1 1 5CA6FFF2
P 4850 3550
F 0 "#PWR058" H 4850 3400 50  0001 C CNN
F 1 "+3.3V" H 4865 3723 50  0000 C CNN
F 2 "" H 4850 3550 50  0001 C CNN
F 3 "" H 4850 3550 50  0001 C CNN
	1    4850 3550
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR063
U 1 1 5CA70504
P 4850 4250
F 0 "#PWR063" H 4850 4000 50  0001 C CNN
F 1 "GND" H 4855 4077 50  0000 C CNN
F 2 "" H 4850 4250 50  0001 C CNN
F 3 "" H 4850 4250 50  0001 C CNN
	1    4850 4250
	1    0    0    -1  
$EndComp
$Comp
L Device:R_US R19
U 1 1 5CA7926C
P 4350 3700
F 0 "R19" H 4418 3746 50  0000 L CNN
F 1 "4.7k" H 4418 3655 50  0000 L CNN
F 2 "Resistor_SMD:R_0805_2012Metric" V 4390 3690 50  0001 C CNN
F 3 "~" H 4350 3700 50  0001 C CNN
F 4 "Stackpole " H 0   0   50  0001 C CNN "DK_Mfr"
F 5 "RMCF0805FT4K70" H 0   0   50  0001 C CNN "DK_Mfr_PN"
F 6 "RMCF0805FT4K70CT-ND" H 0   0   50  0001 C CNN "DK_PN"
F 7 "0805" H 0   0   50  0001 C CNN "Package"
F 8 "any" H 0   0   50  0001 C CNN "Src Any/Spec"
F 9 "1" H 0   0   50  0001 C CNN "Qty Per Unit"
	1    4350 3700
	1    0    0    -1  
$EndComp
$Comp
L Device:CP1 C8
U 1 1 5CA79276
P 4350 4100
F 0 "C8" H 4465 4146 50  0000 L CNN
F 1 "10u" H 4465 4055 50  0000 L CNN
F 2 "Capacitor_SMD:CP_Elec_4x5.4" H 4350 4100 50  0001 C CNN
F 3 "~" H 4350 4100 50  0001 C CNN
F 4 "Würth" H 0   0   50  0001 C CNN "DK_Mfr"
F 5 "865090140001" H 0   0   50  0001 C CNN "DK_Mfr_PN"
F 6 "732-8385-1-ND" H 0   0   50  0001 C CNN "DK_PN"
F 7 "4.0x5.4" H 0   0   50  0001 C CNN "Package"
F 8 "any" H 0   0   50  0001 C CNN "Src Any/Spec"
F 9 "1" H 0   0   50  0001 C CNN "Qty Per Unit"
	1    4350 4100
	1    0    0    -1  
$EndComp
Wire Wire Line
	4350 3850 4350 3900
$Comp
L power:+3.3V #PWR057
U 1 1 5CA79281
P 4350 3550
F 0 "#PWR057" H 4350 3400 50  0001 C CNN
F 1 "+3.3V" H 4365 3723 50  0000 C CNN
F 2 "" H 4350 3550 50  0001 C CNN
F 3 "" H 4350 3550 50  0001 C CNN
	1    4350 3550
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR062
U 1 1 5CA7928B
P 4350 4250
F 0 "#PWR062" H 4350 4000 50  0001 C CNN
F 1 "GND" H 4355 4077 50  0000 C CNN
F 2 "" H 4350 4250 50  0001 C CNN
F 3 "" H 4350 4250 50  0001 C CNN
	1    4350 4250
	1    0    0    -1  
$EndComp
$Comp
L Device:R_US R21
U 1 1 5CA7A869
P 5350 3700
F 0 "R21" H 5418 3746 50  0000 L CNN
F 1 "4.7k" H 5418 3655 50  0000 L CNN
F 2 "Resistor_SMD:R_0805_2012Metric" V 5390 3690 50  0001 C CNN
F 3 "~" H 5350 3700 50  0001 C CNN
F 4 "Stackpole " H 0   0   50  0001 C CNN "DK_Mfr"
F 5 "RMCF0805FT4K70" H 0   0   50  0001 C CNN "DK_Mfr_PN"
F 6 "RMCF0805FT4K70CT-ND" H 0   0   50  0001 C CNN "DK_PN"
F 7 "0805" H 0   0   50  0001 C CNN "Package"
F 8 "any" H 0   0   50  0001 C CNN "Src Any/Spec"
F 9 "1" H 0   0   50  0001 C CNN "Qty Per Unit"
	1    5350 3700
	1    0    0    -1  
$EndComp
$Comp
L Device:CP1 C10
U 1 1 5CA7A873
P 5350 4100
F 0 "C10" H 5465 4146 50  0000 L CNN
F 1 "10u" H 5465 4055 50  0000 L CNN
F 2 "Capacitor_SMD:CP_Elec_4x5.4" H 5350 4100 50  0001 C CNN
F 3 "~" H 5350 4100 50  0001 C CNN
F 4 "Würth" H 0   0   50  0001 C CNN "DK_Mfr"
F 5 "865090140001" H 0   0   50  0001 C CNN "DK_Mfr_PN"
F 6 "732-8385-1-ND" H 0   0   50  0001 C CNN "DK_PN"
F 7 "4.0x5.4" H 0   0   50  0001 C CNN "Package"
F 8 "any" H 0   0   50  0001 C CNN "Src Any/Spec"
F 9 "1" H 0   0   50  0001 C CNN "Qty Per Unit"
	1    5350 4100
	1    0    0    -1  
$EndComp
Wire Wire Line
	5350 3850 5350 3900
$Comp
L power:+3.3V #PWR059
U 1 1 5CA7A87E
P 5350 3550
F 0 "#PWR059" H 5350 3400 50  0001 C CNN
F 1 "+3.3V" H 5365 3723 50  0000 C CNN
F 2 "" H 5350 3550 50  0001 C CNN
F 3 "" H 5350 3550 50  0001 C CNN
	1    5350 3550
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR064
U 1 1 5CA7A888
P 5350 4250
F 0 "#PWR064" H 5350 4000 50  0001 C CNN
F 1 "GND" H 5355 4077 50  0000 C CNN
F 2 "" H 5350 4250 50  0001 C CNN
F 3 "" H 5350 4250 50  0001 C CNN
	1    5350 4250
	1    0    0    -1  
$EndComp
$Comp
L Device:R_US R22
U 1 1 5CA7E2D6
P 5800 3700
F 0 "R22" H 5868 3746 50  0000 L CNN
F 1 "4.7k" H 5868 3655 50  0000 L CNN
F 2 "Resistor_SMD:R_0805_2012Metric" V 5840 3690 50  0001 C CNN
F 3 "~" H 5800 3700 50  0001 C CNN
F 4 "Stackpole " H 0   0   50  0001 C CNN "DK_Mfr"
F 5 "RMCF0805FT4K70" H 0   0   50  0001 C CNN "DK_Mfr_PN"
F 6 "RMCF0805FT4K70CT-ND" H 0   0   50  0001 C CNN "DK_PN"
F 7 "0805" H 0   0   50  0001 C CNN "Package"
F 8 "any" H 0   0   50  0001 C CNN "Src Any/Spec"
F 9 "1" H 0   0   50  0001 C CNN "Qty Per Unit"
	1    5800 3700
	1    0    0    -1  
$EndComp
$Comp
L Device:CP1 C11
U 1 1 5CA7E2E0
P 5800 4100
F 0 "C11" H 5915 4146 50  0000 L CNN
F 1 "10u" H 5915 4055 50  0000 L CNN
F 2 "Capacitor_SMD:CP_Elec_4x5.4" H 5800 4100 50  0001 C CNN
F 3 "~" H 5800 4100 50  0001 C CNN
F 4 "Würth" H 0   0   50  0001 C CNN "DK_Mfr"
F 5 "865090140001" H 0   0   50  0001 C CNN "DK_Mfr_PN"
F 6 "732-8385-1-ND" H 0   0   50  0001 C CNN "DK_PN"
F 7 "4.0x5.4" H 0   0   50  0001 C CNN "Package"
F 8 "any" H 0   0   50  0001 C CNN "Src Any/Spec"
F 9 "1" H 0   0   50  0001 C CNN "Qty Per Unit"
	1    5800 4100
	1    0    0    -1  
$EndComp
Wire Wire Line
	5800 3850 5800 3900
$Comp
L power:+3.3V #PWR060
U 1 1 5CA7E2EB
P 5800 3550
F 0 "#PWR060" H 5800 3400 50  0001 C CNN
F 1 "+3.3V" H 5815 3723 50  0000 C CNN
F 2 "" H 5800 3550 50  0001 C CNN
F 3 "" H 5800 3550 50  0001 C CNN
	1    5800 3550
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR065
U 1 1 5CA7E2F5
P 5800 4250
F 0 "#PWR065" H 5800 4000 50  0001 C CNN
F 1 "GND" H 5805 4077 50  0000 C CNN
F 2 "" H 5800 4250 50  0001 C CNN
F 3 "" H 5800 4250 50  0001 C CNN
	1    5800 4250
	1    0    0    -1  
$EndComp
Wire Wire Line
	4350 3900 4650 3900
Connection ~ 4350 3900
Wire Wire Line
	4350 3900 4350 3950
Wire Wire Line
	4850 3900 5150 3900
Connection ~ 4850 3900
Wire Wire Line
	4850 3900 4850 3950
Wire Wire Line
	5350 3900 5650 3900
Connection ~ 5350 3900
Wire Wire Line
	5350 3900 5350 3950
Wire Wire Line
	5800 3900 6100 3900
Connection ~ 5800 3900
Wire Wire Line
	5800 3900 5800 3950
$Comp
L power:GND #PWR067
U 1 1 5CA81716
P 5450 5300
F 0 "#PWR067" H 5450 5050 50  0001 C CNN
F 1 "GND" H 5455 5127 50  0000 C CNN
F 2 "" H 5450 5300 50  0001 C CNN
F 3 "" H 5450 5300 50  0001 C CNN
	1    5450 5300
	1    0    0    -1  
$EndComp
Wire Wire Line
	5450 5300 5450 5250
Text GLabel 4650 3300 1    50   Output ~ 0
THERM_BED
Text GLabel 5150 3300 1    50   Output ~ 0
THERM_E0
Text GLabel 5650 3300 1    50   Output ~ 0
THERM_E1
Text GLabel 6100 3300 1    50   Output ~ 0
THERM_E2
Wire Wire Line
	4650 3900 4650 3300
Wire Wire Line
	5150 3300 5150 3900
Connection ~ 5150 3900
Wire Wire Line
	5650 3900 5650 3300
Wire Wire Line
	6100 3300 6100 3900
$Comp
L Device:R_US R23
U 1 1 5CA8E651
P 6250 3700
F 0 "R23" H 6318 3746 50  0000 L CNN
F 1 "4.7k" H 6318 3655 50  0000 L CNN
F 2 "Resistor_SMD:R_0805_2012Metric" V 6290 3690 50  0001 C CNN
F 3 "~" H 6250 3700 50  0001 C CNN
F 4 "Stackpole " H 0   0   50  0001 C CNN "DK_Mfr"
F 5 "RMCF0805FT4K70" H 0   0   50  0001 C CNN "DK_Mfr_PN"
F 6 "RMCF0805FT4K70CT-ND" H 0   0   50  0001 C CNN "DK_PN"
F 7 "0805" H 0   0   50  0001 C CNN "Package"
F 8 "any" H 0   0   50  0001 C CNN "Src Any/Spec"
F 9 "1" H 0   0   50  0001 C CNN "Qty Per Unit"
	1    6250 3700
	1    0    0    -1  
$EndComp
$Comp
L Device:CP1 C12
U 1 1 5CA8E65B
P 6250 4100
F 0 "C12" H 6365 4146 50  0000 L CNN
F 1 "10u" H 6365 4055 50  0000 L CNN
F 2 "Capacitor_SMD:CP_Elec_4x5.4" H 6250 4100 50  0001 C CNN
F 3 "~" H 6250 4100 50  0001 C CNN
F 4 "Würth" H 0   0   50  0001 C CNN "DK_Mfr"
F 5 "865090140001" H 0   0   50  0001 C CNN "DK_Mfr_PN"
F 6 "732-8385-1-ND" H 0   0   50  0001 C CNN "DK_PN"
F 7 "4.0x5.4" H 0   0   50  0001 C CNN "Package"
F 8 "any" H 0   0   50  0001 C CNN "Src Any/Spec"
F 9 "1" H 0   0   50  0001 C CNN "Qty Per Unit"
	1    6250 4100
	1    0    0    -1  
$EndComp
Wire Wire Line
	6250 3850 6250 3900
$Comp
L power:+3.3V #PWR061
U 1 1 5CA8E666
P 6250 3550
F 0 "#PWR061" H 6250 3400 50  0001 C CNN
F 1 "+3.3V" H 6265 3723 50  0000 C CNN
F 2 "" H 6250 3550 50  0001 C CNN
F 3 "" H 6250 3550 50  0001 C CNN
	1    6250 3550
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR066
U 1 1 5CA8E670
P 6250 4250
F 0 "#PWR066" H 6250 4000 50  0001 C CNN
F 1 "GND" H 6255 4077 50  0000 C CNN
F 2 "" H 6250 4250 50  0001 C CNN
F 3 "" H 6250 4250 50  0001 C CNN
	1    6250 4250
	1    0    0    -1  
$EndComp
Wire Wire Line
	6250 3900 6550 3900
Connection ~ 6250 3900
Wire Wire Line
	6250 3900 6250 3950
Text GLabel 6550 3300 1    50   Output ~ 0
THERM_ENCL
Wire Wire Line
	6550 3300 6550 3900
$Comp
L Connector_Generic:Conn_02x05_Odd_Even J26
U 1 1 5CA90727
P 5650 5050
F 0 "J26" V 5746 4762 50  0000 R CNN
F 1 "Conn_02x05_Odd_Even" V 5655 4762 50  0000 R CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_2x05_P2.54mm_Vertical" H 5650 5050 50  0001 C CNN
F 3 "~" H 5650 5050 50  0001 C CNN
F 4 "METZ CONNECT USA Inc." H 0   0   50  0001 C CNN "DK_Mfr"
F 5 "PR20205VBDN" H 0   0   50  0001 C CNN "DK_Mfr_PN"
F 6 "1849-1002-ND" H 0   0   50  0001 C CNN "DK_PN"
F 7 "any" H 0   0   50  0001 C CNN "Src Any/Spec"
F 8 "1" H 0   0   50  0001 C CNN "Qty Per Unit"
	1    5650 5050
	0    -1   -1   0   
$EndComp
Wire Wire Line
	5650 5250 5750 5250
Wire Wire Line
	5450 5250 5550 5250
Connection ~ 5450 5250
Wire Wire Line
	5550 5250 5650 5250
Connection ~ 5550 5250
Connection ~ 5650 5250
Wire Wire Line
	5750 5250 5850 5250
Connection ~ 5750 5250
Wire Wire Line
	5650 3900 5650 4750
Connection ~ 5650 3900
Wire Wire Line
	5150 4550 5550 4550
Wire Wire Line
	5550 4550 5550 4750
Wire Wire Line
	5150 3900 5150 4550
Wire Wire Line
	5450 4750 5450 4600
Wire Wire Line
	5450 4600 4650 4600
Wire Wire Line
	4650 4600 4650 3900
Connection ~ 4650 3900
Wire Wire Line
	5750 4750 5750 4550
Wire Wire Line
	5750 4550 6100 4550
Wire Wire Line
	6100 4550 6100 3900
Connection ~ 6100 3900
Wire Wire Line
	5850 4750 5850 4600
Wire Wire Line
	5850 4600 6550 4600
Wire Wire Line
	6550 4600 6550 3900
Connection ~ 6550 3900
$EndSCHEMATC
