EESchema Schematic File Version 4
LIBS:TeensyPrinter-cache
EELAYER 29 0
EELAYER END
$Descr USLetter 11000 8500
encoding utf-8
Sheet 3 10
Title "Teensy Pololu Shield"
Date "2019-03-28"
Rev "0.1"
Comp "Scott Alfter"
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L pololu:POLOLU U?
U 1 1 5C89EFEA
P 3950 1800
AR Path="/5C876DD3/5C89EFEA" Ref="U?"  Part="1" 
AR Path="/5C897698/5C89EFEA" Ref="U5"  Part="1" 
F 0 "U5" H 3950 2547 60  0000 C CNN
F 1 "E0" H 3950 2441 60  0000 C CNN
F 2 "pololu:pololu" H 3950 1800 60  0001 C CNN
F 3 "" H 3950 1800 60  0000 C CNN
F 4 "Samtec Inc." H 0   0   50  0001 C CNN "DK_Mfr"
F 5 "SSA-117-S-T" H 0   0   50  0001 C CNN "DK_Mfr_PN"
F 6 "SAM1122-17-ND" H 0   0   50  0001 C CNN "DK_PN"
F 7 "any" H 0   0   50  0001 C CNN "Src Any/Spec"
F 8 "1" H 0   0   50  0001 C CNN "Qty Per Unit"
	1    3950 1800
	1    0    0    -1  
$EndComp
Text GLabel 2350 1600 0    50   Input ~ 0
E0_STEP
Text GLabel 2350 1700 0    50   Input ~ 0
E0_DIR
Text GLabel 2350 1400 0    50   Input ~ 0
E0_EN_B
$Comp
L Device:R_US R?
U 1 1 5C89EFF4
P 3000 1250
AR Path="/5C876DD3/5C89EFF4" Ref="R?"  Part="1" 
AR Path="/5C897698/5C89EFF4" Ref="R7"  Part="1" 
F 0 "R7" H 2850 1300 50  0000 L CNN
F 1 "10k" H 2800 1200 50  0000 L CNN
F 2 "Resistor_SMD:R_0805_2012Metric" V 3040 1240 50  0001 C CNN
F 3 "~" H 3000 1250 50  0001 C CNN
F 4 "Stackpole " H 0   0   50  0001 C CNN "DK_Mfr"
F 5 "RMCF0805FT10K0" H 0   0   50  0001 C CNN "DK_Mfr_PN"
F 6 "RMCF0805FT10K0CT-ND" H 0   0   50  0001 C CNN "DK_PN"
F 7 "0805" H 0   0   50  0001 C CNN "Package"
F 8 "any" H 0   0   50  0001 C CNN "Src Any/Spec"
F 9 "1" H 0   0   50  0001 C CNN "Qty Per Unit"
	1    3000 1250
	1    0    0    -1  
$EndComp
Wire Wire Line
	3000 1400 3150 1400
Connection ~ 3000 1400
Wire Wire Line
	3000 1050 3000 1100
Wire Wire Line
	3150 1250 3150 1300
$Comp
L power:+12V #PWR?
U 1 1 5C89F00B
P 4750 1250
AR Path="/5C876DD3/5C89F00B" Ref="#PWR?"  Part="1" 
AR Path="/5C897698/5C89F00B" Ref="#PWR024"  Part="1" 
F 0 "#PWR024" H 4750 1100 50  0001 C CNN
F 1 "+12V" H 4765 1423 50  0000 C CNN
F 2 "" H 4750 1250 50  0001 C CNN
F 3 "" H 4750 1250 50  0001 C CNN
	1    4750 1250
	1    0    0    -1  
$EndComp
Wire Wire Line
	4750 1250 4750 1300
$Comp
L conns:Conn_Generic_3x04 J?
U 1 1 5C89F012
P 2650 2200
AR Path="/5C876DD3/5C89F012" Ref="J?"  Part="1" 
AR Path="/5C897698/5C89F012" Ref="J8"  Part="1" 
F 0 "J8" H 2750 2450 50  0000 C CNN
F 1 "Conn_Generic_3x04" H 2650 2587 50  0001 C CNN
F 2 "conns:PinHeader_3x04_P2.54mm_Vertical" H 2650 1800 50  0001 C CNN
F 3 "" H 2650 1800 50  0001 C CNN
F 4 "Samtec" H 0   0   50  0001 C CNN "DK_Mfr"
F 5 "TSW-104-07-L-T" H 0   0   50  0001 C CNN "DK_Mfr_PN"
F 6 "SAM12310-ND" H 0   0   50  0001 C CNN "DK_PN"
F 7 "any" H 0   0   50  0001 C CNN "Src Any/Spec"
F 8 "1" H 0   0   50  0001 C CNN "Qty Per Unit"
	1    2650 2200
	1    0    0    -1  
$EndComp
Wire Wire Line
	2700 1900 3150 1900
Wire Wire Line
	2600 1900 2600 1800
Wire Wire Line
	2600 1800 3050 1800
Wire Wire Line
	2600 2500 2600 2550
Wire Wire Line
	2600 2550 2950 2550
Wire Wire Line
	2950 2550 2950 2000
Wire Wire Line
	2950 2000 3150 2000
Wire Wire Line
	2700 2500 3000 2500
Wire Wire Line
	3000 2500 3000 2200
Wire Wire Line
	3000 2200 3150 2200
Wire Wire Line
	3150 2300 3100 2300
Wire Wire Line
	3100 2300 3100 2250
Wire Wire Line
	3100 1300 3150 1300
Connection ~ 3150 1300
Wire Wire Line
	2900 2050 2900 2150
Wire Wire Line
	2900 2150 2900 2250
Connection ~ 2900 2150
Wire Wire Line
	2900 2250 2900 2350
Connection ~ 2900 2250
Wire Wire Line
	2900 2250 3100 2250
Connection ~ 3100 2250
Wire Wire Line
	3100 2250 3100 1300
Text GLabel 2350 2150 0    50   Input ~ 0
TMC_SCK
Text GLabel 2350 2250 0    50   Input ~ 0
E0_CS
Wire Wire Line
	2350 2150 2400 2150
Wire Wire Line
	2350 2250 2400 2250
$Comp
L Device:R_US R?
U 1 1 5C89F037
P 3050 2450
AR Path="/5C876DD3/5C89F037" Ref="R?"  Part="1" 
AR Path="/5C897698/5C89F037" Ref="R8"  Part="1" 
F 0 "R8" H 3100 2500 50  0000 L CNN
F 1 "100k" H 3100 2400 50  0000 L CNN
F 2 "Resistor_SMD:R_0805_2012Metric" V 3090 2440 50  0001 C CNN
F 3 "~" H 3050 2450 50  0001 C CNN
F 4 "Stackpole " H 0   0   50  0001 C CNN "DK_Mfr"
F 5 "RMCF0805FT100K" H 0   0   50  0001 C CNN "DK_Mfr_PN"
F 6 "RMCF0805FT100KCT-ND" H 0   0   50  0001 C CNN "DK_PN"
F 7 "0805" H 0   0   50  0001 C CNN "Package"
F 8 "any" H 0   0   50  0001 C CNN "Src Any/Spec"
F 9 "1" H 0   0   50  0001 C CNN "Qty Per Unit"
	1    3050 2450
	1    0    0    -1  
$EndComp
Wire Wire Line
	3050 2300 3050 1800
Connection ~ 3050 1800
Wire Wire Line
	3050 1800 3150 1800
$Comp
L power:GND #PWR?
U 1 1 5C89F041
P 3050 2650
AR Path="/5C876DD3/5C89F041" Ref="#PWR?"  Part="1" 
AR Path="/5C897698/5C89F041" Ref="#PWR026"  Part="1" 
F 0 "#PWR026" H 3050 2400 50  0001 C CNN
F 1 "GND" H 3055 2477 50  0000 C CNN
F 2 "" H 3050 2650 50  0001 C CNN
F 3 "" H 3050 2650 50  0001 C CNN
	1    3050 2650
	1    0    0    -1  
$EndComp
Wire Wire Line
	3050 2650 3050 2600
$Comp
L Connector_Generic:Conn_01x04 J?
U 1 1 5C89F048
P 5000 1500
AR Path="/5C876DD3/5C89F048" Ref="J?"  Part="1" 
AR Path="/5C897698/5C89F048" Ref="J7"  Part="1" 
F 0 "J7" H 5080 1492 50  0000 L CNN
F 1 "E0_MOT" H 5080 1401 50  0000 L CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x04_P2.54mm_Vertical" H 5000 1500 50  0001 C CNN
F 3 "~" H 5000 1500 50  0001 C CNN
F 4 "Sullins" H 0   0   50  0001 C CNN "DK_Mfr"
F 5 "PRPC004SAAN-RC" H 0   0   50  0001 C CNN "DK_Mfr_PN"
F 6 "S1011EC-04-ND" H 0   0   50  0001 C CNN "DK_PN"
F 7 "any" H 0   0   50  0001 C CNN "Src Any/Spec"
F 8 "1" H 0   0   50  0001 C CNN "Qty Per Unit"
	1    5000 1500
	1    0    0    -1  
$EndComp
Wire Wire Line
	4750 1400 4800 1400
Wire Wire Line
	4750 1500 4800 1500
Wire Wire Line
	4750 1600 4800 1600
Wire Wire Line
	4750 1700 4800 1700
$Comp
L power:GND #PWR?
U 1 1 5C89F055
P 4750 2650
AR Path="/5C876DD3/5C89F055" Ref="#PWR?"  Part="1" 
AR Path="/5C897698/5C89F055" Ref="#PWR027"  Part="1" 
F 0 "#PWR027" H 4750 2400 50  0001 C CNN
F 1 "GND" H 4755 2477 50  0000 C CNN
F 2 "" H 4750 2650 50  0001 C CNN
F 3 "" H 4750 2650 50  0001 C CNN
	1    4750 2650
	1    0    0    -1  
$EndComp
Wire Wire Line
	4750 2200 4750 2300
Wire Wire Line
	4750 2300 4750 2650
Connection ~ 4750 2300
Wire Wire Line
	2350 1400 3000 1400
Wire Wire Line
	2350 1600 3150 1600
Wire Wire Line
	2350 1700 3150 1700
$Comp
L pololu:POLOLU U?
U 1 1 5C8AAF3D
P 4000 4100
AR Path="/5C876DD3/5C8AAF3D" Ref="U?"  Part="1" 
AR Path="/5C897698/5C8AAF3D" Ref="U6"  Part="1" 
F 0 "U6" H 4000 4847 60  0000 C CNN
F 1 "E1" H 4000 4741 60  0000 C CNN
F 2 "pololu:pololu" H 4000 4100 60  0001 C CNN
F 3 "" H 4000 4100 60  0000 C CNN
F 4 "Samtec Inc." H 100 0   50  0001 C CNN "DK_Mfr"
F 5 "SSA-117-S-T" H 100 0   50  0001 C CNN "DK_Mfr_PN"
F 6 "SAM1122-17-ND" H 100 0   50  0001 C CNN "DK_PN"
F 7 "any" H 100 0   50  0001 C CNN "Src Any/Spec"
F 8 "1" H 100 0   50  0001 C CNN "Qty Per Unit"
	1    4000 4100
	1    0    0    -1  
$EndComp
Text GLabel 2400 3900 0    50   Input ~ 0
E1_STEP
Text GLabel 2400 4000 0    50   Input ~ 0
E1_DIR
Text GLabel 2400 3700 0    50   Input ~ 0
E1_EN_B
$Comp
L Device:R_US R?
U 1 1 5C8AAF47
P 3050 3550
AR Path="/5C876DD3/5C8AAF47" Ref="R?"  Part="1" 
AR Path="/5C897698/5C8AAF47" Ref="R9"  Part="1" 
F 0 "R9" H 2900 3600 50  0000 L CNN
F 1 "10k" H 2850 3500 50  0000 L CNN
F 2 "Resistor_SMD:R_0805_2012Metric" V 3090 3540 50  0001 C CNN
F 3 "~" H 3050 3550 50  0001 C CNN
F 4 "Stackpole " H 100 0   50  0001 C CNN "DK_Mfr"
F 5 "RMCF0805FT10K0" H 100 0   50  0001 C CNN "DK_Mfr_PN"
F 6 "RMCF0805FT10K0CT-ND" H 100 0   50  0001 C CNN "DK_PN"
F 7 "0805" H 100 0   50  0001 C CNN "Package"
F 8 "any" H 100 0   50  0001 C CNN "Src Any/Spec"
F 9 "1" H 100 0   50  0001 C CNN "Qty Per Unit"
	1    3050 3550
	1    0    0    -1  
$EndComp
Wire Wire Line
	3050 3700 3200 3700
Connection ~ 3050 3700
Wire Wire Line
	3050 3350 3050 3400
Wire Wire Line
	3200 3550 3200 3600
$Comp
L power:+12V #PWR?
U 1 1 5C8AAF5E
P 4800 3550
AR Path="/5C876DD3/5C8AAF5E" Ref="#PWR?"  Part="1" 
AR Path="/5C897698/5C8AAF5E" Ref="#PWR031"  Part="1" 
F 0 "#PWR031" H 4800 3400 50  0001 C CNN
F 1 "+12V" H 4815 3723 50  0000 C CNN
F 2 "" H 4800 3550 50  0001 C CNN
F 3 "" H 4800 3550 50  0001 C CNN
	1    4800 3550
	1    0    0    -1  
$EndComp
Wire Wire Line
	4800 3550 4800 3600
$Comp
L conns:Conn_Generic_3x04 J?
U 1 1 5C8AAF65
P 2700 4500
AR Path="/5C876DD3/5C8AAF65" Ref="J?"  Part="1" 
AR Path="/5C897698/5C8AAF65" Ref="J10"  Part="1" 
F 0 "J10" H 2800 4750 50  0000 C CNN
F 1 "Conn_Generic_3x04" H 2700 4887 50  0001 C CNN
F 2 "conns:PinHeader_3x04_P2.54mm_Vertical" H 2700 4100 50  0001 C CNN
F 3 "" H 2700 4100 50  0001 C CNN
F 4 "Samtec" H 100 0   50  0001 C CNN "DK_Mfr"
F 5 "TSW-104-07-L-T" H 100 0   50  0001 C CNN "DK_Mfr_PN"
F 6 "SAM12310-ND" H 100 0   50  0001 C CNN "DK_PN"
F 7 "any" H 100 0   50  0001 C CNN "Src Any/Spec"
F 8 "1" H 100 0   50  0001 C CNN "Qty Per Unit"
	1    2700 4500
	1    0    0    -1  
$EndComp
Wire Wire Line
	2750 4200 3200 4200
Wire Wire Line
	2650 4200 2650 4100
Wire Wire Line
	2650 4100 3100 4100
Wire Wire Line
	2650 4800 2650 4850
Wire Wire Line
	2650 4850 3000 4850
Wire Wire Line
	3000 4850 3000 4300
Wire Wire Line
	3000 4300 3200 4300
Wire Wire Line
	2750 4800 3050 4800
Wire Wire Line
	3050 4800 3050 4500
Wire Wire Line
	3050 4500 3200 4500
Wire Wire Line
	3200 4600 3150 4600
Wire Wire Line
	3150 4600 3150 4550
Wire Wire Line
	3150 3600 3200 3600
Connection ~ 3200 3600
Wire Wire Line
	2950 4350 2950 4450
Wire Wire Line
	2950 4450 2950 4550
Connection ~ 2950 4450
Wire Wire Line
	2950 4550 2950 4650
Connection ~ 2950 4550
Wire Wire Line
	2950 4550 3150 4550
Connection ~ 3150 4550
Wire Wire Line
	3150 4550 3150 3600
Text GLabel 2400 4450 0    50   Input ~ 0
TMC_SCK
Text GLabel 2400 4550 0    50   Input ~ 0
E1_CS
Wire Wire Line
	2400 4450 2450 4450
Wire Wire Line
	2400 4550 2450 4550
$Comp
L Device:R_US R?
U 1 1 5C8AAF8A
P 3100 4750
AR Path="/5C876DD3/5C8AAF8A" Ref="R?"  Part="1" 
AR Path="/5C897698/5C8AAF8A" Ref="R10"  Part="1" 
F 0 "R10" H 3150 4800 50  0000 L CNN
F 1 "100k" H 3150 4700 50  0000 L CNN
F 2 "Resistor_SMD:R_0805_2012Metric" V 3140 4740 50  0001 C CNN
F 3 "~" H 3100 4750 50  0001 C CNN
F 4 "Stackpole " H 100 0   50  0001 C CNN "DK_Mfr"
F 5 "RMCF0805FT100K" H 100 0   50  0001 C CNN "DK_Mfr_PN"
F 6 "RMCF0805FT100KCT-ND" H 100 0   50  0001 C CNN "DK_PN"
F 7 "0805" H 100 0   50  0001 C CNN "Package"
F 8 "any" H 100 0   50  0001 C CNN "Src Any/Spec"
F 9 "1" H 100 0   50  0001 C CNN "Qty Per Unit"
	1    3100 4750
	1    0    0    -1  
$EndComp
Wire Wire Line
	3100 4600 3100 4100
Connection ~ 3100 4100
Wire Wire Line
	3100 4100 3200 4100
$Comp
L power:GND #PWR?
U 1 1 5C8AAF94
P 3100 4950
AR Path="/5C876DD3/5C8AAF94" Ref="#PWR?"  Part="1" 
AR Path="/5C897698/5C8AAF94" Ref="#PWR033"  Part="1" 
F 0 "#PWR033" H 3100 4700 50  0001 C CNN
F 1 "GND" H 3105 4777 50  0000 C CNN
F 2 "" H 3100 4950 50  0001 C CNN
F 3 "" H 3100 4950 50  0001 C CNN
	1    3100 4950
	1    0    0    -1  
$EndComp
Wire Wire Line
	3100 4950 3100 4900
$Comp
L Connector_Generic:Conn_01x04 J?
U 1 1 5C8AAF9B
P 5050 3800
AR Path="/5C876DD3/5C8AAF9B" Ref="J?"  Part="1" 
AR Path="/5C897698/5C8AAF9B" Ref="J9"  Part="1" 
F 0 "J9" H 5130 3792 50  0000 L CNN
F 1 "E1_MOT" H 5130 3701 50  0000 L CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x04_P2.54mm_Vertical" H 5050 3800 50  0001 C CNN
F 3 "~" H 5050 3800 50  0001 C CNN
F 4 "Sullins" H 100 0   50  0001 C CNN "DK_Mfr"
F 5 "PRPC004SAAN-RC" H 100 0   50  0001 C CNN "DK_Mfr_PN"
F 6 "S1011EC-04-ND" H 100 0   50  0001 C CNN "DK_PN"
F 7 "any" H 100 0   50  0001 C CNN "Src Any/Spec"
F 8 "1" H 100 0   50  0001 C CNN "Qty Per Unit"
	1    5050 3800
	1    0    0    -1  
$EndComp
Wire Wire Line
	4800 3700 4850 3700
Wire Wire Line
	4800 3800 4850 3800
Wire Wire Line
	4800 3900 4850 3900
Wire Wire Line
	4800 4000 4850 4000
$Comp
L power:GND #PWR?
U 1 1 5C8AAFA6
P 4800 4950
AR Path="/5C876DD3/5C8AAFA6" Ref="#PWR?"  Part="1" 
AR Path="/5C897698/5C8AAFA6" Ref="#PWR034"  Part="1" 
F 0 "#PWR034" H 4800 4700 50  0001 C CNN
F 1 "GND" H 4805 4777 50  0000 C CNN
F 2 "" H 4800 4950 50  0001 C CNN
F 3 "" H 4800 4950 50  0001 C CNN
	1    4800 4950
	1    0    0    -1  
$EndComp
Wire Wire Line
	4800 4500 4800 4600
Wire Wire Line
	4800 4600 4800 4950
Connection ~ 4800 4600
Wire Wire Line
	2400 3700 3050 3700
Wire Wire Line
	2400 3900 3200 3900
Wire Wire Line
	2400 4000 3200 4000
Text GLabel 2400 6200 0    50   Input ~ 0
E2_STEP
Text GLabel 2400 6300 0    50   Input ~ 0
E2_DIR
Text GLabel 2400 6000 0    50   Input ~ 0
E2_EN_B
$Comp
L Device:R_US R?
U 1 1 5C8AC67E
P 3050 5850
AR Path="/5C876DD3/5C8AC67E" Ref="R?"  Part="1" 
AR Path="/5C897698/5C8AC67E" Ref="R11"  Part="1" 
F 0 "R11" H 2900 5900 50  0000 L CNN
F 1 "10k" H 2850 5800 50  0000 L CNN
F 2 "Resistor_SMD:R_0805_2012Metric" V 3090 5840 50  0001 C CNN
F 3 "~" H 3050 5850 50  0001 C CNN
F 4 "Stackpole " H 150 50  50  0001 C CNN "DK_Mfr"
F 5 "RMCF0805FT10K0" H 150 50  50  0001 C CNN "DK_Mfr_PN"
F 6 "RMCF0805FT10K0CT-ND" H 150 50  50  0001 C CNN "DK_PN"
F 7 "0805" H 150 50  50  0001 C CNN "Package"
F 8 "any" H 150 50  50  0001 C CNN "Src Any/Spec"
F 9 "1" H 150 50  50  0001 C CNN "Qty Per Unit"
	1    3050 5850
	1    0    0    -1  
$EndComp
Wire Wire Line
	3050 6000 3200 6000
Connection ~ 3050 6000
Wire Wire Line
	3050 5650 3050 5700
Wire Wire Line
	3200 5850 3200 5900
$Comp
L power:+12V #PWR?
U 1 1 5C8AC695
P 4800 5850
AR Path="/5C876DD3/5C8AC695" Ref="#PWR?"  Part="1" 
AR Path="/5C897698/5C8AC695" Ref="#PWR038"  Part="1" 
F 0 "#PWR038" H 4800 5700 50  0001 C CNN
F 1 "+12V" H 4815 6023 50  0000 C CNN
F 2 "" H 4800 5850 50  0001 C CNN
F 3 "" H 4800 5850 50  0001 C CNN
	1    4800 5850
	1    0    0    -1  
$EndComp
Wire Wire Line
	4800 5850 4800 5900
$Comp
L conns:Conn_Generic_3x04 J?
U 1 1 5C8AC69C
P 2700 6800
AR Path="/5C876DD3/5C8AC69C" Ref="J?"  Part="1" 
AR Path="/5C897698/5C8AC69C" Ref="J12"  Part="1" 
F 0 "J12" H 2800 7050 50  0000 C CNN
F 1 "Conn_Generic_3x04" H 2700 7187 50  0001 C CNN
F 2 "conns:PinHeader_3x04_P2.54mm_Vertical" H 2700 6400 50  0001 C CNN
F 3 "" H 2700 6400 50  0001 C CNN
F 4 "Samtec" H 150 50  50  0001 C CNN "DK_Mfr"
F 5 "TSW-104-07-L-T" H 150 50  50  0001 C CNN "DK_Mfr_PN"
F 6 "SAM12310-ND" H 150 50  50  0001 C CNN "DK_PN"
F 7 "any" H 150 50  50  0001 C CNN "Src Any/Spec"
F 8 "1" H 150 50  50  0001 C CNN "Qty Per Unit"
	1    2700 6800
	1    0    0    -1  
$EndComp
Wire Wire Line
	2750 6500 3200 6500
Wire Wire Line
	2650 6500 2650 6400
Wire Wire Line
	2650 6400 3100 6400
Wire Wire Line
	2650 7100 2650 7150
Wire Wire Line
	2650 7150 3000 7150
Wire Wire Line
	3000 7150 3000 6600
Wire Wire Line
	3000 6600 3200 6600
Wire Wire Line
	2750 7100 3050 7100
Wire Wire Line
	3050 7100 3050 6800
Wire Wire Line
	3050 6800 3200 6800
Wire Wire Line
	3200 6900 3150 6900
Wire Wire Line
	3150 6900 3150 6850
Wire Wire Line
	3150 5900 3200 5900
Wire Wire Line
	2950 6650 2950 6750
Wire Wire Line
	2950 6750 2950 6850
Connection ~ 2950 6750
Wire Wire Line
	2950 6850 2950 6950
Connection ~ 2950 6850
Wire Wire Line
	2950 6850 3150 6850
Connection ~ 3150 6850
Wire Wire Line
	3150 6850 3150 5900
Text GLabel 2400 6750 0    50   Input ~ 0
TMC_SCK
Text GLabel 2400 6850 0    50   Input ~ 0
E2_CS
Wire Wire Line
	2400 6750 2450 6750
Wire Wire Line
	2400 6850 2450 6850
$Comp
L Device:R_US R?
U 1 1 5C8AC6C1
P 3100 7050
AR Path="/5C876DD3/5C8AC6C1" Ref="R?"  Part="1" 
AR Path="/5C897698/5C8AC6C1" Ref="R12"  Part="1" 
F 0 "R12" H 3150 7100 50  0000 L CNN
F 1 "100k" H 3150 7000 50  0000 L CNN
F 2 "Resistor_SMD:R_0805_2012Metric" V 3140 7040 50  0001 C CNN
F 3 "~" H 3100 7050 50  0001 C CNN
F 4 "Stackpole " H 150 50  50  0001 C CNN "DK_Mfr"
F 5 "RMCF0805FT100K" H 150 50  50  0001 C CNN "DK_Mfr_PN"
F 6 "RMCF0805FT100KCT-ND" H 150 50  50  0001 C CNN "DK_PN"
F 7 "0805" H 150 50  50  0001 C CNN "Package"
F 8 "any" H 150 50  50  0001 C CNN "Src Any/Spec"
F 9 "1" H 150 50  50  0001 C CNN "Qty Per Unit"
	1    3100 7050
	1    0    0    -1  
$EndComp
Wire Wire Line
	3100 6900 3100 6400
Connection ~ 3100 6400
Wire Wire Line
	3100 6400 3200 6400
$Comp
L power:GND #PWR?
U 1 1 5C8AC6CB
P 3100 7250
AR Path="/5C876DD3/5C8AC6CB" Ref="#PWR?"  Part="1" 
AR Path="/5C897698/5C8AC6CB" Ref="#PWR040"  Part="1" 
F 0 "#PWR040" H 3100 7000 50  0001 C CNN
F 1 "GND" H 3105 7077 50  0000 C CNN
F 2 "" H 3100 7250 50  0001 C CNN
F 3 "" H 3100 7250 50  0001 C CNN
	1    3100 7250
	1    0    0    -1  
$EndComp
Wire Wire Line
	3100 7250 3100 7200
$Comp
L Connector_Generic:Conn_01x04 J?
U 1 1 5C8AC6D2
P 5050 6100
AR Path="/5C876DD3/5C8AC6D2" Ref="J?"  Part="1" 
AR Path="/5C897698/5C8AC6D2" Ref="J11"  Part="1" 
F 0 "J11" H 5130 6092 50  0000 L CNN
F 1 "E2_MOT" H 5130 6001 50  0000 L CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x04_P2.54mm_Vertical" H 5050 6100 50  0001 C CNN
F 3 "~" H 5050 6100 50  0001 C CNN
F 4 "Sullins" H 150 50  50  0001 C CNN "DK_Mfr"
F 5 "PRPC004SAAN-RC" H 150 50  50  0001 C CNN "DK_Mfr_PN"
F 6 "S1011EC-04-ND" H 150 50  50  0001 C CNN "DK_PN"
F 7 "any" H 150 50  50  0001 C CNN "Src Any/Spec"
F 8 "1" H 150 50  50  0001 C CNN "Qty Per Unit"
	1    5050 6100
	1    0    0    -1  
$EndComp
Wire Wire Line
	4800 6000 4850 6000
Wire Wire Line
	4800 6100 4850 6100
Wire Wire Line
	4800 6200 4850 6200
Wire Wire Line
	4800 6300 4850 6300
$Comp
L power:GND #PWR?
U 1 1 5C8AC6DD
P 4800 7250
AR Path="/5C876DD3/5C8AC6DD" Ref="#PWR?"  Part="1" 
AR Path="/5C897698/5C8AC6DD" Ref="#PWR041"  Part="1" 
F 0 "#PWR041" H 4800 7000 50  0001 C CNN
F 1 "GND" H 4805 7077 50  0000 C CNN
F 2 "" H 4800 7250 50  0001 C CNN
F 3 "" H 4800 7250 50  0001 C CNN
	1    4800 7250
	1    0    0    -1  
$EndComp
Wire Wire Line
	2400 6000 3050 6000
Wire Wire Line
	2400 6200 3200 6200
Wire Wire Line
	2400 6300 3200 6300
$Comp
L Device:CP1 C4
U 1 1 5C8CCBA9
P 3950 2800
F 0 "C4" H 4065 2846 50  0000 L CNN
F 1 "100uF 35V" H 4065 2755 50  0000 L CNN
F 2 "Capacitor_SMD:CP_Elec_6.3x7.7" H 3950 2800 50  0001 C CNN
F 3 "~" H 3950 2800 50  0001 C CNN
F 4 "Kemet" H 0   0   50  0001 C CNN "DK_Mfr"
F 5 "EDK107M035A9HAA" H 0   0   50  0001 C CNN "DK_Mfr_PN"
F 6 "399-6671-1-ND" H 0   0   50  0001 C CNN "DK_PN"
F 7 "6.3x7.7" H 0   0   50  0001 C CNN "Package"
F 8 "any" H 0   0   50  0001 C CNN "Src Any/Spec"
F 9 "1" H 0   0   50  0001 C CNN "Qty Per Unit"
	1    3950 2800
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR028
U 1 1 5C8CCBB0
P 3950 3000
F 0 "#PWR028" H 3950 2750 50  0001 C CNN
F 1 "GND" H 3955 2827 50  0000 C CNN
F 2 "" H 3950 3000 50  0001 C CNN
F 3 "" H 3950 3000 50  0001 C CNN
	1    3950 3000
	1    0    0    -1  
$EndComp
Wire Wire Line
	3950 2600 3950 2650
Wire Wire Line
	3950 2950 3950 3000
$Comp
L Device:CP1 C5
U 1 1 5C8D02E8
P 4000 5100
F 0 "C5" H 4115 5146 50  0000 L CNN
F 1 "100uF 35V" H 4115 5055 50  0000 L CNN
F 2 "Capacitor_SMD:CP_Elec_6.3x7.7" H 4000 5100 50  0001 C CNN
F 3 "~" H 4000 5100 50  0001 C CNN
F 4 "Kemet" H 100 0   50  0001 C CNN "DK_Mfr"
F 5 "EDK107M035A9HAA" H 100 0   50  0001 C CNN "DK_Mfr_PN"
F 6 "399-6671-1-ND" H 100 0   50  0001 C CNN "DK_PN"
F 7 "6.3x7.7" H 100 0   50  0001 C CNN "Package"
F 8 "any" H 100 0   50  0001 C CNN "Src Any/Spec"
F 9 "1" H 100 0   50  0001 C CNN "Qty Per Unit"
	1    4000 5100
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR035
U 1 1 5C8D02EF
P 4000 5300
F 0 "#PWR035" H 4000 5050 50  0001 C CNN
F 1 "GND" H 4005 5127 50  0000 C CNN
F 2 "" H 4000 5300 50  0001 C CNN
F 3 "" H 4000 5300 50  0001 C CNN
	1    4000 5300
	1    0    0    -1  
$EndComp
Wire Wire Line
	4000 4900 4000 4950
Wire Wire Line
	4000 5250 4000 5300
$Comp
L Device:CP1 C6
U 1 1 5C8D3B5D
P 4000 7400
F 0 "C6" H 4115 7446 50  0000 L CNN
F 1 "100uF 35V" H 4115 7355 50  0000 L CNN
F 2 "Capacitor_SMD:CP_Elec_6.3x7.7" H 4000 7400 50  0001 C CNN
F 3 "~" H 4000 7400 50  0001 C CNN
F 4 "Kemet" H 150 50  50  0001 C CNN "DK_Mfr"
F 5 "EDK107M035A9HAA" H 150 50  50  0001 C CNN "DK_Mfr_PN"
F 6 "399-6671-1-ND" H 150 50  50  0001 C CNN "DK_PN"
F 7 "6.3x7.7" H 150 50  50  0001 C CNN "Package"
F 8 "any" H 150 50  50  0001 C CNN "Src Any/Spec"
F 9 "1" H 150 50  50  0001 C CNN "Qty Per Unit"
	1    4000 7400
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR042
U 1 1 5C8D3B64
P 4000 7600
F 0 "#PWR042" H 4000 7350 50  0001 C CNN
F 1 "GND" H 4005 7427 50  0000 C CNN
F 2 "" H 4000 7600 50  0001 C CNN
F 3 "" H 4000 7600 50  0001 C CNN
	1    4000 7600
	1    0    0    -1  
$EndComp
Wire Wire Line
	4000 7200 4000 7250
Wire Wire Line
	4000 7550 4000 7600
$Comp
L power:+12V #PWR?
U 1 1 5C90B825
P 3950 2600
AR Path="/5C876DD3/5C90B825" Ref="#PWR?"  Part="1" 
AR Path="/5C897698/5C90B825" Ref="#PWR032"  Part="1" 
F 0 "#PWR032" H 3950 2450 50  0001 C CNN
F 1 "+12V" H 3965 2773 50  0000 C CNN
F 2 "" H 3950 2600 50  0001 C CNN
F 3 "" H 3950 2600 50  0001 C CNN
	1    3950 2600
	1    0    0    -1  
$EndComp
$Comp
L power:+12V #PWR?
U 1 1 5C90BD3D
P 4000 4900
AR Path="/5C876DD3/5C90BD3D" Ref="#PWR?"  Part="1" 
AR Path="/5C897698/5C90BD3D" Ref="#PWR039"  Part="1" 
F 0 "#PWR039" H 4000 4750 50  0001 C CNN
F 1 "+12V" H 4015 5073 50  0000 C CNN
F 2 "" H 4000 4900 50  0001 C CNN
F 3 "" H 4000 4900 50  0001 C CNN
	1    4000 4900
	1    0    0    -1  
$EndComp
$Comp
L power:+12V #PWR?
U 1 1 5C90C975
P 4000 7200
AR Path="/5C876DD3/5C90C975" Ref="#PWR?"  Part="1" 
AR Path="/5C897698/5C90C975" Ref="#PWR048"  Part="1" 
F 0 "#PWR048" H 4000 7050 50  0001 C CNN
F 1 "+12V" H 4015 7373 50  0000 C CNN
F 2 "" H 4000 7200 50  0001 C CNN
F 3 "" H 4000 7200 50  0001 C CNN
	1    4000 7200
	1    0    0    -1  
$EndComp
$Comp
L power:+3.3V #PWR029
U 1 1 5C99BC88
P 3000 1050
F 0 "#PWR029" H 3000 900 50  0001 C CNN
F 1 "+3.3V" H 3015 1223 50  0000 C CNN
F 2 "" H 3000 1050 50  0001 C CNN
F 3 "" H 3000 1050 50  0001 C CNN
	1    3000 1050
	1    0    0    -1  
$EndComp
$Comp
L power:+3.3V #PWR030
U 1 1 5C99C797
P 3150 1250
F 0 "#PWR030" H 3150 1100 50  0001 C CNN
F 1 "+3.3V" H 3165 1423 50  0000 C CNN
F 2 "" H 3150 1250 50  0001 C CNN
F 3 "" H 3150 1250 50  0001 C CNN
	1    3150 1250
	1    0    0    -1  
$EndComp
$Comp
L power:+3.3V #PWR036
U 1 1 5C99CB0E
P 3050 3350
F 0 "#PWR036" H 3050 3200 50  0001 C CNN
F 1 "+3.3V" H 3065 3523 50  0000 C CNN
F 2 "" H 3050 3350 50  0001 C CNN
F 3 "" H 3050 3350 50  0001 C CNN
	1    3050 3350
	1    0    0    -1  
$EndComp
$Comp
L power:+3.3V #PWR037
U 1 1 5C99D4E7
P 3200 3550
F 0 "#PWR037" H 3200 3400 50  0001 C CNN
F 1 "+3.3V" H 3215 3723 50  0000 C CNN
F 2 "" H 3200 3550 50  0001 C CNN
F 3 "" H 3200 3550 50  0001 C CNN
	1    3200 3550
	1    0    0    -1  
$EndComp
$Comp
L power:+3.3V #PWR046
U 1 1 5C99D80D
P 3050 5650
F 0 "#PWR046" H 3050 5500 50  0001 C CNN
F 1 "+3.3V" H 3065 5823 50  0000 C CNN
F 2 "" H 3050 5650 50  0001 C CNN
F 3 "" H 3050 5650 50  0001 C CNN
	1    3050 5650
	1    0    0    -1  
$EndComp
$Comp
L power:+3.3V #PWR047
U 1 1 5C99E061
P 3200 5850
F 0 "#PWR047" H 3200 5700 50  0001 C CNN
F 1 "+3.3V" H 3215 6023 50  0000 C CNN
F 2 "" H 3200 5850 50  0001 C CNN
F 3 "" H 3200 5850 50  0001 C CNN
	1    3200 5850
	1    0    0    -1  
$EndComp
Text GLabel 5150 2000 2    50   Output ~ 0
X_MAX
$Comp
L Connector_Generic:Conn_01x02 J?
U 1 1 5CC2D5FC
P 5050 2200
AR Path="/5C876DD3/5CC2D5FC" Ref="J?"  Part="1" 
AR Path="/5C897698/5CC2D5FC" Ref="J37"  Part="1" 
F 0 "J37" V 4968 2280 50  0000 L CNN
F 1 "Conn_01x02" V 5013 2280 50  0001 L CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x02_P2.54mm_Vertical" H 5050 2200 50  0001 C CNN
F 3 "~" H 5050 2200 50  0001 C CNN
F 4 "Amphenol ICC (FCI)" H 0   0   50  0001 C CNN "DK_Mfr"
F 5 "77311-818-02LF" H 0   0   50  0001 C CNN "DK_Mfr_PN"
F 6 "609-4938-ND" H 0   0   50  0001 C CNN "DK_PN"
F 7 "any" H 0   0   50  0001 C CNN "Src Any/Spec"
F 8 "1" H 0   0   50  0001 C CNN "Qty Per Unit"
	1    5050 2200
	0    1    1    0   
$EndComp
Wire Wire Line
	4750 2000 4950 2000
Wire Wire Line
	5050 2000 5150 2000
Text GLabel 5200 4300 2    50   Output ~ 0
Y_MAX
$Comp
L Connector_Generic:Conn_01x02 J?
U 1 1 5CC31C58
P 5100 4500
AR Path="/5C876DD3/5CC31C58" Ref="J?"  Part="1" 
AR Path="/5C897698/5CC31C58" Ref="J38"  Part="1" 
F 0 "J38" V 5018 4580 50  0000 L CNN
F 1 "Conn_01x02" V 5063 4580 50  0001 L CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x02_P2.54mm_Vertical" H 5100 4500 50  0001 C CNN
F 3 "~" H 5100 4500 50  0001 C CNN
F 4 "Amphenol ICC (FCI)" H 100 0   50  0001 C CNN "DK_Mfr"
F 5 "77311-818-02LF" H 100 0   50  0001 C CNN "DK_Mfr_PN"
F 6 "609-4938-ND" H 100 0   50  0001 C CNN "DK_PN"
F 7 "any" H 100 0   50  0001 C CNN "Src Any/Spec"
F 8 "1" H 100 0   50  0001 C CNN "Qty Per Unit"
	1    5100 4500
	0    1    1    0   
$EndComp
Wire Wire Line
	4800 4300 5000 4300
Wire Wire Line
	5100 4300 5200 4300
Wire Wire Line
	4800 6900 4800 7250
Wire Wire Line
	4800 6800 4800 6900
Connection ~ 4800 6900
Connection ~ 3200 5900
$Comp
L pololu:POLOLU U?
U 1 1 5C8AC674
P 4000 6400
AR Path="/5C876DD3/5C8AC674" Ref="U?"  Part="1" 
AR Path="/5C897698/5C8AC674" Ref="U7"  Part="1" 
F 0 "U7" H 4000 7147 60  0000 C CNN
F 1 "E2" H 4000 7041 60  0000 C CNN
F 2 "pololu:pololu" H 4000 6400 60  0001 C CNN
F 3 "" H 4000 6400 60  0000 C CNN
F 4 "Samtec Inc." H 150 50  50  0001 C CNN "DK_Mfr"
F 5 "SSA-117-S-T" H 150 50  50  0001 C CNN "DK_Mfr_PN"
F 6 "SAM1122-17-ND" H 150 50  50  0001 C CNN "DK_PN"
F 7 "any" H 150 50  50  0001 C CNN "Src Any/Spec"
F 8 "1" H 150 50  50  0001 C CNN "Qty Per Unit"
	1    4000 6400
	1    0    0    -1  
$EndComp
Text GLabel 5200 6600 2    50   Output ~ 0
Z_MAX
$Comp
L Connector_Generic:Conn_01x02 J?
U 1 1 5CC36889
P 5100 6800
AR Path="/5C876DD3/5CC36889" Ref="J?"  Part="1" 
AR Path="/5C897698/5CC36889" Ref="J39"  Part="1" 
F 0 "J39" V 5018 6880 50  0000 L CNN
F 1 "Conn_01x02" V 5063 6880 50  0001 L CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x02_P2.54mm_Vertical" H 5100 6800 50  0001 C CNN
F 3 "~" H 5100 6800 50  0001 C CNN
F 4 "Amphenol ICC (FCI)" H 150 50  50  0001 C CNN "DK_Mfr"
F 5 "77311-818-02LF" H 150 50  50  0001 C CNN "DK_Mfr_PN"
F 6 "609-4938-ND" H 150 50  50  0001 C CNN "DK_PN"
F 7 "any" H 150 50  50  0001 C CNN "Src Any/Spec"
F 8 "1" H 150 50  50  0001 C CNN "Qty Per Unit"
	1    5100 6800
	0    1    1    0   
$EndComp
Wire Wire Line
	4800 6600 5000 6600
Wire Wire Line
	5100 6600 5200 6600
Text Notes 5950 4250 0    50   ~ 0
Endstop connections on this page are provided in case one or more of the driver\nslots is repurposed for a multi-Z-motor setup (as for automatic bed leveling)...\nif using as extruder drivers, leave the endstop jumpers empty.
Text GLabel 1500 2150 2    50   Input ~ 0
TMC_SDI
Text GLabel 1500 2250 2    50   Output ~ 0
TMC_SDO
$Comp
L Connector_Generic:Conn_01x03 J?
U 1 1 5CABB54B
P 1300 2350
AR Path="/5C876DD3/5CABB54B" Ref="J?"  Part="1" 
AR Path="/5C897698/5CABB54B" Ref="J51"  Part="1" 
F 0 "J51" H 1450 2350 50  0000 C CNN
F 1 "Conn_01x03" H 1218 2576 50  0001 C CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x03_P2.54mm_Vertical" H 1300 2350 50  0001 C CNN
F 3 "~" H 1300 2350 50  0001 C CNN
	1    1300 2350
	-1   0    0    -1  
$EndComp
$Comp
L Connector_Generic:Conn_01x03 J?
U 1 1 5CABB552
P 1300 2050
AR Path="/5C876DD3/5CABB552" Ref="J?"  Part="1" 
AR Path="/5C897698/5CABB552" Ref="J50"  Part="1" 
F 0 "J50" H 1450 2050 50  0000 C CNN
F 1 "Conn_01x03" H 1218 2276 50  0001 C CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x03_P2.54mm_Vertical" H 1300 2050 50  0001 C CNN
F 3 "~" H 1300 2050 50  0001 C CNN
	1    1300 2050
	-1   0    0    1   
$EndComp
Text GLabel 1500 1950 2    50   UnSpc ~ 0
TMC_SD_DC
Text GLabel 1500 4450 2    50   Input ~ 0
TMC_SDI
Text GLabel 1500 4550 2    50   Output ~ 0
TMC_SDO
$Comp
L Connector_Generic:Conn_01x03 J?
U 1 1 5CAC057C
P 1300 4650
AR Path="/5C876DD3/5CAC057C" Ref="J?"  Part="1" 
AR Path="/5C897698/5CAC057C" Ref="J53"  Part="1" 
F 0 "J53" H 1450 4650 50  0000 C CNN
F 1 "Conn_01x03" H 1218 4876 50  0001 C CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x03_P2.54mm_Vertical" H 1300 4650 50  0001 C CNN
F 3 "~" H 1300 4650 50  0001 C CNN
	1    1300 4650
	-1   0    0    -1  
$EndComp
Wire Wire Line
	1500 4650 2450 4650
$Comp
L Connector_Generic:Conn_01x03 J?
U 1 1 5CAC0583
P 1300 4350
AR Path="/5C876DD3/5CAC0583" Ref="J?"  Part="1" 
AR Path="/5C897698/5CAC0583" Ref="J52"  Part="1" 
F 0 "J52" H 1450 4350 50  0000 C CNN
F 1 "Conn_01x03" H 1218 4576 50  0001 C CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x03_P2.54mm_Vertical" H 1300 4350 50  0001 C CNN
F 3 "~" H 1300 4350 50  0001 C CNN
	1    1300 4350
	-1   0    0    1   
$EndComp
Wire Wire Line
	1500 4350 2450 4350
Wire Wire Line
	1500 2450 1500 4250
Wire Wire Line
	2400 6950 2450 6950
Text GLabel 2400 6950 0    50   Output ~ 0
TMC_SDO
Text GLabel 1500 6750 2    50   Input ~ 0
TMC_SDI
$Comp
L Connector_Generic:Conn_01x03 J?
U 1 1 5CAD2FC1
P 1300 6650
AR Path="/5C876DD3/5CAD2FC1" Ref="J?"  Part="1" 
AR Path="/5C897698/5CAD2FC1" Ref="J54"  Part="1" 
F 0 "J54" H 1450 6650 50  0000 C CNN
F 1 "Conn_01x03" H 1218 6876 50  0001 C CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x03_P2.54mm_Vertical" H 1300 6650 50  0001 C CNN
F 3 "~" H 1300 6650 50  0001 C CNN
	1    1300 6650
	-1   0    0    1   
$EndComp
Wire Wire Line
	1500 6650 2450 6650
Wire Wire Line
	1500 4750 1500 6550
Wire Wire Line
	1500 2350 2400 2350
Wire Wire Line
	2400 2050 1500 2050
$EndSCHEMATC
